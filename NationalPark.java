import java.util.Scanner;
public class NationalPark {
    public static void main(String[] args) {
        Dog[] aPackOfDog = new Dog[4];
        Scanner sc =new Scanner(System.in);
        for (int i = 0; i < 4; i++) {
            System.out.println("Please enter your dog's species");
            String species = sc.nextLine();
            System.out.println("Please enter the dog's age");
            int age = sc.nextInt();
            System.out.println("Please enter the dog's size in meters");
            double size = sc.nextDouble();
            aPackOfDog[i] =new Dog(species, age, size);
            sc.nextLine();
        }
        System.out.println("Please reset the values for the last dog");
        System.out.println("Please enter your last dog's species");
        String species = sc.nextLine();
        aPackOfDog[3].setSpecies(species);
        System.out.println("Please enter the last dog's age");
        int age = sc.nextInt();
        aPackOfDog[3].setAge(age);
        System.out.println("Please enter the last dog's size in meters");
        double size = sc.nextDouble();
        aPackOfDog[3].setSize(size);

        System.out.println("The dog is " + aPackOfDog[3].getSpecies()+", it is " + aPackOfDog[3].getAge() + " years old,  it is " + aPackOfDog[3].getSize() + " meters.");
        aPackOfDog[0].jump();
        aPackOfDog[0].run();
        aPackOfDog[1].grow(0.8);
    }
}
