public class Dog {
    private String species;
    private int age;
    private double size;

    public Dog(String species, int age, double size) {
        this.species = species;
        this.age = age;
        this.size = size;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public void run(){
        System.out.println("The " + species + " runs for " + 10 * size + " meters!");
    }
    public void jump(){
        System.out.println("The " + species + " jumps for " + 2 * age + " centimeters!" );
    }
    public void grow(double size){
        if(validate(size)){
            this.size=size;
            System.out.println("The " + species + " has grown to " + this.size +" meters!");
        }else{
            System.out.println("You have entered an invalid value");
        }
    }
    public boolean validate(double size){
        return size > this.size;
    }
}
